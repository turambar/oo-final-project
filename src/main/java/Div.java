class Div implements Expression {
    private Expression lhs;
    private Expression rhs;

    Div(Expression lhs, Expression rhs) {
        this.lhs = lhs;
        this.rhs = rhs;
    }

    @Override
    public Expression eval(Variable variable, double value) {
        return new Div(lhs.eval(variable, value), rhs.eval(variable, value));
    }

    @Override
    public double eval() {
        return lhs.eval() / rhs.eval();
    }

    @Override
    public Expression deriv(Variable derivedVariable) {
        return new Div(
                new Subtract(
                        new Multiply(lhs.deriv(derivedVariable), rhs),
                        new Multiply(lhs, rhs.deriv(derivedVariable))
                ),
                new Multiply(rhs, rhs)
        );
    }
}
