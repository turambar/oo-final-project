interface Expression {
    Expression eval(Variable variable, double value);
    double eval();
    Expression deriv(Variable derivedVariable);
}
