class NumericalValue implements Expression {
    private double value;

    NumericalValue(double value) {
        this.value = value;
    }

    @Override
    public Expression eval(Variable variable, double value) {
        return new NumericalValue(this.value);
    }

    @Override
    public double eval() {
        return value;
    }

    @Override
    public Expression deriv(Variable variable) {
        return new NumericalValue(0);
    }
}
