class Sum implements Expression {
    private Expression lhs;
    private Expression rhs;

    Sum(Expression lhs, Expression rhs) {
        this.rhs = rhs;
        this.lhs = lhs;
    }

    @Override
    public Expression eval(Variable variable, double value) {
        return new Sum(lhs.eval(variable, value), rhs.eval(variable, value));
    }

    @Override
    public double eval() {
        return rhs.eval() + lhs.eval();
    }

    @Override
    public Expression deriv(Variable derivedVariable) {
        return new Sum(lhs.deriv(derivedVariable), rhs.deriv(derivedVariable));
    }
}
