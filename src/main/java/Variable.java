class Variable implements Expression {
    private String name;
    private double value;

    private Variable(String name, double value) {
        this.name = name;
        this.value = value;
    }

    Variable(String name) {
        this.name = name;
    }

    @Override
    public double eval() {
        return this.value;
    }

    @Override
    public Expression eval(Variable variable, double value) {
        if (variable.name.equals(this.name))
            return new NumericalValue(value);

        return new Variable(this.name, this.value);
    }

    @Override
    public Expression deriv(Variable derivedVariable) {
        if (derivedVariable.name.equals(this.name))
            return new NumericalValue(1);

        return new NumericalValue(0);
    }
}
