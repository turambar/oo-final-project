class Multiply implements Expression {
    private Expression lhs;
    private Expression rhs;

    Multiply(Expression lhs, Expression rhs) {
        this.lhs = lhs;
        this.rhs = rhs;
    }

    @Override
    public Expression eval(Variable variable, double value) {
        return new Multiply(lhs.eval(variable, value), rhs.eval(variable, value));
    }

    @Override
    public double eval() {
        return lhs.eval() * rhs.eval();
    }

    @Override
    public Expression deriv(Variable derivedVariable) {
        return new Sum(new Multiply(lhs.deriv(derivedVariable), rhs), new Multiply(lhs, rhs.deriv(derivedVariable)));
    }
}
