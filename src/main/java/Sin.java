class Sin implements Expression {
    private Expression argument;

    Sin(Expression argument) {
        this.argument = argument;
    }

    @Override
    public Expression eval(Variable variable, double value) {
        return new Sin(argument.eval(variable, value));
    }

    @Override
    public double eval() {
        return Math.sin(argument.eval());
    }

    @Override
    public Expression deriv(Variable derivedVariable) {
        return new Multiply(
                new Cos(this.argument),
                this.argument.deriv(derivedVariable)
        );
    }
}
