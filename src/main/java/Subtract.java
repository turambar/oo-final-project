class Subtract implements Expression {
    private Expression rhs;
    private Expression lhs;

    Subtract(Expression lhs, Expression rhs) {
        this.lhs = lhs;
        this.rhs = rhs;
    }

    @Override
    public Expression eval(Variable variable, double value) {
        return new Subtract(lhs.eval(variable, value), rhs.eval(variable, value));
    }

    @Override
    public double eval() {
        return lhs.eval() - rhs.eval();
    }

    @Override
    public Expression deriv(Variable derivedVariable) {
        return new Subtract(lhs.deriv(derivedVariable), rhs.deriv(derivedVariable));
    }
}
