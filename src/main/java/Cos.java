class Cos implements Expression {
    private Expression argument;

    Cos(Expression argument) {
        this.argument = argument;
    }

    @Override
    public Expression eval(Variable variable, double value) {
        return new Cos(argument.eval(variable, value));
    }

    @Override
    public double eval() {
        return Math.cos(argument.eval());
    }

    @Override
    public Expression deriv(Variable derivedVariable) {
        return new Multiply(
                new Multiply(new NumericalValue(-1), new Sin(this.argument)),
                this.argument.deriv(derivedVariable)
        );
    }
}
