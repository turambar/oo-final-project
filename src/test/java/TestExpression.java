import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class TestExpression {

    @Test
    void DescriptionTestCase1() {
        Variable x = new Variable("x");
        Variable y = new Variable("y");

        Expression exp = new Div(new Subtract(new Cos(x), new NumericalValue(1)), y);

        assertEquals(0, exp.eval(y, 2).deriv(x).eval(x, 0).eval(), 1E-5);
    }

    @Test
    void DescriptionTestCase2() {
        Variable x = new Variable("x");
        Variable y = new Variable("y");

        Expression exp = new Div(new Subtract(new Cos(x), new NumericalValue(1)), y);

        Expression yEval = exp.eval(y, 2);
        Expression derived = yEval.deriv(x);
        Expression xEval = derived.eval(x, 0);
        xEval = derived.eval(x, Math.PI / 2);
        assertEquals(-0.5, xEval.eval(), 1E-5);
    }

    @Test
    void SumEvalTest() {
        Variable y = new Variable("y");
        Variable x = new Variable("x");

        Expression exp = new Sum(x, y);

        assertEquals(5, exp.eval(y, 2).eval(x, 3).eval());
        assertEquals(9, exp.eval(y, 5).eval(x, 4).eval());
    }

    @Test
    void SubtractEvalTest() {
        Variable y = new Variable("y");
        Variable x = new Variable("x");

        Expression exp = new Subtract(x, y);

        assertEquals(1, exp.eval(y, 2).eval(x, 3).eval());
        assertEquals(-1, exp.eval(y, 5).eval(x, 4).eval());
    }

    @Test
    void DivEvalTest() {
        Variable y = new Variable("y");
        Variable x = new Variable("x");

        Expression exp = new Div(x, y);

        assertEquals(.3 / .2, exp.eval(y, 2).eval(x, 3).eval(), 1E-5);
        assertEquals(.4 / .5, exp.eval(y, 5).eval(x, 4).eval(), 1E-5);
    }

    @Test
    void MultiplyEvalTest() {
        Variable y = new Variable("y");
        Variable x = new Variable("x");

        Expression exp = new Multiply(x, y);

        assertEquals(6, exp.eval(y, 2).eval(x, 3).eval());
        assertEquals(20, exp.eval(y, 5).eval(x, 4).eval());
    }

    @Test
    void SinEvalTest() {
        Variable x = new Variable("x");

        Expression exp = new Sin(x);

        assertEquals(0, exp.eval(x, 0).eval(), 1E-5);
        assertEquals(1, exp.eval(x, Math.PI / 2).eval(), 1E-5);
    }

    @Test
    void CosEvalTest() {
        Variable x = new Variable("x");

        Expression exp = new Cos(x);

        assertEquals(1, exp.eval(x, 0).eval(), 1E-5);
        assertEquals(0, exp.eval(x, Math.PI / 2).eval(), 1E-5);
    }

    @Test
    void SinDerivEvalTest() {
        Variable x = new Variable("x");

        Expression exp = new Sin(x);

        assertEquals(1, exp.deriv(x).eval(x, 0).eval(), 1E-5);
        assertEquals(0, exp.deriv(x).eval(x, Math.PI / 2).eval(), 1E-5);
    }

    @Test
    void CosDerivEvalTest() {
        Variable x = new Variable("x");

        Expression exp = new Cos(x);

        assertEquals(0, exp.deriv(x).eval(x, 0).eval(), 1E-5);
        assertEquals(-1, exp.deriv(x).eval(x, Math.PI / 2).eval(), 1E-5);
    }

    @Test
    void MultiplyDerivationTest() {
        Variable x = new Variable("x");

        Expression exp = new Multiply(new Sin(x), new Cos(x));

        assertEquals(1, exp.deriv(x).eval(x, 0).eval(), 1E-5);
        assertEquals(-1, exp.deriv(x).eval(x, Math.PI / 2).eval(), 1E-5);
    }

    @Test
    void DivDerivationTest() {
        Variable x = new Variable("x");

        Expression exp = new Div(new Sin(x), new Cos(x));

        assertEquals(4, exp.deriv(x).eval(x, Math.PI / 3).eval(), 1E-5);
    }

    @Test
    void SumDerivationTest() {
        Variable x = new Variable("x");

        Expression exp = new Sum(new Sin(x), new Cos(x));

        assertEquals(0, exp.deriv(x).eval(x, Math.PI / 4).eval(), 1E-5);
        assertEquals(1, exp.deriv(x).eval(x, 0).eval(), 1E-5);
        assertEquals(-1, exp.deriv(x).eval(x, Math.PI / 2).eval(), 1E-5);
    }

    @Test
    void SubtractDerivationTest() {
        Variable x = new Variable("x");

        Expression exp = new Subtract(new Sin(x), new Cos(x));

        assertEquals(Math.sqrt(2), exp.deriv(x).eval(x, Math.PI / 4).eval(), 1E-5);
        assertEquals(1, exp.deriv(x).eval(x, 0).eval(), 1E-5);
        assertEquals(1, exp.deriv(x).eval(x, Math.PI / 2).eval(), 1E-5);
    }
}
