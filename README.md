# OO Final Project

Library for evaluate mathematical expressions.

## Run Tests By Maven

Use the package manager [maven](https://https://maven.apache.org/) to build and run the test cases.

```bash
mvn test
```
